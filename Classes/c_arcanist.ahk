#Persistent
;
; Arcanist
;
cast_arc_bane:
  global ARC_Bane
  send_control(ARC_Bane)
  return

cast_arc_bio:
  global ARC_Bio
  send_control(ARC_Bio)
  return

cast_arc_bio2:
  global ARC_Bio2
  send_control(ARC_Bio2)
  return

cast_arc_energydrain:
  global ARC_EnergyDrain
  send_control(ARC_EnergyDrain)
  return

cast_arc_eyeforaneye:
  global ARC_EyeForAnEye
  send_control(ARC_EyeForAnEye)
  return

cast_arc_miasma:
  global ARC_Miasma
  send_control(ARC_Miasma)
  return

cast_arc_miasma2:
  global ARC_Miasma2
  send_control(ARC_Miasma2)
  return

cast_arc_physick:
  global ARC_Physick
  send_control(ARC_Physick)
  return

cast_arc_resurrection:
  global ARC_Resurrection
  send_control(ARC_Resurrection)
  return

cast_arc_rouse:
  global ARC_Rouse
  send_control(ARC_Rouse)
  return

cast_arc_ruin:
  global ARC_Ruin
  send_control(ARC_Ruin)
  return

cast_arc_ruin2:
  global ARC_Ruin2
  send_control(ARC_Ruin2)
  return

cast_arc_shadowflare:
  global ARC_ShadowFlare
  send_control(ARC_ShadowFlare)
  return

cast_arc_summon:
  global ARC_Summon
  send_control(ARC_Summon)
  return

cast_arc_summon2:
  global ARC_Summon2
  send_control(ARC_Summon2)
  return

cast_arc_sustain:
  global ARC_Sustain
  send_control(ARC_Sustain)
  return

cast_arc_virus:
  global ARC_Virus
  send_control(ARC_Virus)
  return
