#Persistent
;
; Archer
;
cast_arc_heavyshot:
	global ARC_HeavyShot
	send_control(ARC_HeavyShot)
	return

cast_arc_straightshot:
	global ARC_StraightShot
	send_control(ARC_StraightShot)
	return

cast_arc_ragingstrikes:
	global ARC_RagingStrikes
	send_control(ARC_RagingStrikes)
	return

cast_arc_miserysend:
	global ARC_MiserysEnd
	send_control(ARC_RagingStrikes)
	return

cast_arc_shadowbind:
	global ARC_Shadowbind
	send_control(ARC_RagingStrikes)
	return

cast_arc_venomousbite:
	global ARC_VenomousBite
	send_control(ARC_RagingStrikes)
	return

cast_arc_bloodletter:
	global ARC_Bloodletter
	send_control(ARC_RagingStrikes)
	return

cast_arc_repellingshot:
	global ARC_RepellingShot
	send_control(ARC_RagingStrikes)
	return
	
cast_arc_quicknock:
	global ARC_QuickNock
	send_control(ARC_RagingStrikes)
	return

cast_arc_swiftsong:
	global ARC_Swiftsong
	send_control(ARC_RagingStrikes)
	return

cast_arc_hawkseye:
	global ARC_HawksEye
	send_control(ARC_RagingStrikes)
	return

cast_arc_windbite:
	global ARC_Windbite
	send_control(ARC_RagingStrikes)
	return

cast_arc_quellingstrikes:
	global ARC_QuellingStrikes
	send_control(ARC_RagingStrikes)
	return

cast_arc_barrage:
	global ARC_Barrage
	send_control(ARC_RagingStrikes)
	return

cast_arc_bluntarrow:
	global ARC_BluntArrow
	send_control(ARC_RagingStrikes)
	return

cast_arc_flamingarrow:
	global ARC_FlamingArrow
	send_control(ARC_RagingStrikes)
	return

cast_arc_widevolley:
	global ARC_WideVolley
	send_control(ARC_RagingStrikes)
	return
