#Persistent
;
; Bard
;
cast_brd_armyspaeon:
  global BRD_ArmysPaeon
  send_control(BRD_ArmysPaeon)
  return

cast_brd_battlevoice:
  global BRD_BattleVoice
  send_control(BRD_BattleVoice)
  return

cast_brd_foerequiem:
  global BRD_FoeRequiem
  send_control(BRD_FoeRequiem)
  return

cast_brd_magesballad:
  global BRD_MagesBallad
  send_control(BRD_MagesBallad)
  return

cast_brd_rainofdeath:
  global BRD_RainOfDeath
  send_control(BRD_RainOfDeath)
  return
