#Persistent
;
; Blackmage
;
cast_bmg_apocatastasis:
  global BMG_Apocatastasis
  send_control(BMG_Apocatastasis)
  return

cast_bmg_convert:
  global BMG_Convert
  send_control(BMG_Convert)
  return

cast_bmg_flare:
  global BMG_Flare
  send_control(BMG_Flare)
  return

cast_bmg_freeze:
  global BMG_Freeze
  send_control(BMG_Freeze)
  return

cast_bmg_manawall:
  global BMG_Manawall
  send_control(BMG_Manawall)
  return
