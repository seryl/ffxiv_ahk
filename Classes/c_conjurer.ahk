#Persistent
;
; Conjurer
;
cast_cnj_aero:
	global CNJ_Aero
	send_control(CNJ_Aero)
	return

cast_cnj_aero2:
	global CNJ_Aero2
	send_control(CNJ_Aero2)
	return

cast_cnj_clericstance:
	global CNJ_ClericStance
	send_control(CNJ_ClericStance)
	return

cast_cnj_cure:
	global CNJ_Cure
	send_control(CNJ_Cure)
	return

cast_cnj_cure2:
	global CNJ_Cure2
	send_control(CNJ_Cure2)
	return

cast_cnj_cure3:
	global CNJ_Cure3
	send_control(CNJ_Cure3)
	return

cast_cnj_esuna:
	global CNJ_Esuna
	send_control(CNJ_Esuna)
	return

cast_cnj_fluidaura:
	global CNJ_FluidAura
	send_control(CNJ_FluidAura)
	return

cast_cnj_medica:
	global CNJ_Medica
	send_control(CNJ_Medica)
	return

cast_cnj_medica2:
	global CNJ_Medica2
	send_control(CNJ_Medica2)
	return

cast_cnj_protect:
	global CNJ_Protect
	send_control(CNJ_Protect)
	return

cast_cnj_raise:
	global CNJ_Raise
	send_control(CNJ_Raise)
	return

cast_cnj_repose:
	global CNJ_Repose
	send_control(CNJ_Repose)
	return

cast_cnj_shroudofsaints:
	global CNJ_ShroudOfSaints
	send_control(CNJ_ShroudOfSaints)
	return

cast_cnj_stone:
	global CNJ_Stone
	send_control(CNJ_Stone)
	return

cast_cnj_stone2:
	global CNJ_Stone2
	send_control(CNJ_Stone2)
	return

cast_cnj_stoneskin:
	global CNJ_Stoneskin
	send_control(CNJ_Stoneskin)
	return
