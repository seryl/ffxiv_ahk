#Persistent
;
; Dragoon
;
cast_drg_dragonfiredive:
  global DRG_DragonfireDive
  send_control(DRG_DragonfireDive)
  return

cast_drg_elusivejump:
  global DRG_ElusiveJump
  send_control(DRG_ElusiveJump)
  return

cast_drg_jump:
  global DRG_Jump
  send_control(DRG_Jump)
  return

cast_drg_powersurge:
  global DRG_PowerSurge
  send_control(DRG_PowerSurge)
  return

cast_drg_spineshatterdive:
  global DRG_SpineshatterDive
  send_control(DRG_SpineshatterDive)
  return
