#Persistent
;
; Gladiator
;
cast_gla_awareness:
  global GLA_Awareness
  send_control(GLA_Awareness)
  return

cast_gla_bulwark:
  global GLA_Bulwark
  send_control(GLA_Bulwark)
  return

cast_gla_circleofscorn:
  global GLA_CircleOfScorn
  send_control(GLA_CircleOfScorn)
  return

cast_gla_convalesence:
  global GLA_Convalesence
  send_control(GLA_Convalesence)
  return

cast_gla_fastblade:
  global GLA_FastBlade
  send_control(GLA_FastBlade)
  return

cast_gla_fightorflight:
  global GLA_FightOrFlight
  send_control(GLA_FightOrFlight)
  return

cast_gla_flash:
  global GLA_Flash
  send_control(GLA_Flash)
  return

cast_gla_provoke:
  global GLA_Provoke
  send_control(GLA_Provoke)
  return

cast_gla_rageofhalone:
  global GLA_RageOfHalone
  send_control(GLA_RageOfHalone)
  return

cast_gla_rampart:
  global GLA_Rampart
  send_control(GLA_Rampart)
  return

cast_gla_riotblade:
  global GLA_RiotBlade
  send_control(GLA_RiotBlade)
  return

cast_gla_savageblade:
  global GLA_SavageBlade
  send_control(GLA_SavageBlade)
  return

cast_gla_sentinel:
  global GLA_Sentinel
  send_control(GLA_Sentinel)
  return

cast_gla_shieldbash:
  global GLA_ShieldBash
  send_control(GLA_ShieldBash)
  return

cast_gla_shieldlob:
  global GLA_ShieldLob
  send_control(GLA_ShieldLob)
  return

cast_gla_shieldswipe:
  global GLA_ShieldSwipe
  send_control(GLA_ShieldSwipe)
  return

cast_gla_temperedwill:
  global GLA_TemperedWill
  send_control(GLA_TemperedWill)
  return
