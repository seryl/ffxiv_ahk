#Persistent
;
; Lancer
;
cast_lnc_bloodforblood:
  global LNC_BloodForBlood
  send_control(LNC_BloodForBlood)
  return

cast_lnc_chaosthrust:
  global LNC_ChaosThrust
  send_control(LNC_ChaosThrust)
  return

cast_lnc_disembowel:
  global LNC_Disembowel
  send_control(LNC_Disembowel)
  return

cast_lnc_doomspike:
  global LNC_DoomSpike
  send_control(LNC_DoomSpike)
  return

cast_lnc_feint:
  global LNC_Feint
  send_control(LNC_Feint)
  return

cast_lnc_fullthrust:
  global LNC_FullThrust
  send_control(LNC_FullThrust)
  return

cast_lnc_heavythrust:
  global LNC_HeavyThrust
  send_control(LNC_HeavyThrust)
  return

cast_lnc_impulsedrive:
  global LNC_ImpulseDrive
  send_control(LNC_ImpulseDrive)
  return

cast_lnc_invigorate:
  global LNC_Invigorate
  send_control(LNC_Invigorate)
  return

cast_lnc_keenflurry:
  global LNC_KeenFlurry
  send_control(LNC_KeenFlurry)
  return

cast_lnc_legsweep:
  global LNC_LegSweep
  send_control(LNC_LegSweep)
  return

cast_lnc_lifesurge:
  global LNC_LifeSurge
  send_control(LNC_LifeSurge)
  return

cast_lnc_phlebotomize:
  global LNC_Phlebotomize
  send_control(LNC_Phlebotomize)
  return

cast_lnc_piercingtalon:
  global LNC_PiercingTalon
  send_control(LNC_PiercingTalon)
  return

cast_lnc_ringofthorns:
  global LNC_RingOfThorns
  send_control(LNC_RingOfThorns)
  return

cast_lnc_truethrust:
  global LNC_TrueThrust
  send_control(LNC_TrueThrust)
  return

cast_lnc_vorpalthrust:
  global LNC_VorpalThrust
  send_control(LNC_VorpalThrust)
  return
