#Persistent
;
; Marauder
;
cast_mar_berserk:
	global MAR_Berserk
	send_control(MAR_Berserk)
	return

cast_mar_bloodbath:
	global MAR_Bloodbath
	send_control(MAR_Bloodbath)
	return

cast_mar_brutalswing:
	global MAR_BrutalSwing
	send_control(MAR_BrutalSwing)
	return

cast_mar_butchersblock:
  global MAR_ButchersBlock
  send_control(MAR_ButchersBlock)
  return

cast_mar_foresight:
  global MAR_Foresight
  send_control(MAR_Foresight)
  return

cast_mar_fracture:
	global MAR_Fracture
	send_control(MAR_Fracture)
	return

cast_mar_heavyswing:
	global MAR_HeavySwing
	send_control(MAR_HeavySwing)
	return

cast_mar_maim:
	global MAR_Maim
	send_control(MAR_Maim)
	return

cast_mar_mercystroke:
	global MAR_MercyStroke
	send_control(MAR_MercyStroke)
	return

cast_mar_overpower:
	global MAR_Overpower
	send_control(MAR_Overpower)
	return

cast_mar_skullsunder:
	global MAR_SkullSunder
	send_control(MAR_SkullSunder)
	return

cast_mar_thrillofbattle:
	global MAR_ThrillOfBattle
	send_control(MAR_ThrillOfBattle)
	return

cast_mar_tomahawk:
	global MAR_Tomahawk
	send_control(MAR_Tomahawk)
	return
