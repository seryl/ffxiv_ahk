#Persistent
;
; Monk
;
cast_mnk_dragonkick:
  global MNK_DragonKick
  send_control(MNK_DragonKick)
  return

cast_mnk_fistsoffire:
  global MNK_FistsOfFire
  send_control(MNK_FistsOfFire)
  return

cast_mnk_oneilmpunch:
  global MNK_OneIlmPunch
  send_control(MNK_OneIlmPunch)
  return

cast_mnk_rockbreaker:
  global MNK_Rockbreaker
  send_control(MNK_Rockbreaker)
  return

cast_mnk_shouldertackle:
  global MNK_ShoulderTackle
  send_control(MNK_ShoulderTackle)
  return
