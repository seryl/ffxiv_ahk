#Persistent
;
; Paladin
;
cast_pal_cover:
  global PAL_Cover
  send_control(PAL_Cover)
  return

cast_pal_hallowedground:
  global PAL_HallowedGround
  send_control(PAL_HallowedGround)
  return

cast_pal_shieldoath:
  global PAL_ShieldOath
  send_control(PAL_ShieldOath)
  return

cast_pal_spiritswithin:
  global PAL_SpiritsWithin
  send_control(PAL_SpiritsWithin)
  return

cast_pal_swordoath:
  global PAL_SwordOath
  send_control(PAL_SwordOath)
  return
