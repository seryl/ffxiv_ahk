#Persistent
;
; Pugilist
;
cast_pug_armofthedestroyer:
	global PUG_ArmOfTheDestroyer
	send_control(PUG_ArmOfTheDestroyer)
	return

cast_pug_bootshine:
	global PUG_Bootshine
	send_control(PUG_Bootshine)
	return

cast_pug_demolish:
	global PUG_Demolish
	send_control(PUG_Demolish)
	return

cast_pug_featherfoot:
	global PUG_Featherfoot
	send_control(PUG_Featherfoot)
	return

cast_pug_fistsofearth:
	global PUG_FistsOfEarth
	send_control(PUG_FistsOfEarth)
	return

cast_pug_fistsofwind:
	global PUG_FistsOfWind
	send_control(PUG_FistsOfWind)
	return

cast_pug_haymaker:
	global PUG_Haymaker
	send_control(PUG_Haymaker)
	return

cast_pug_howlingfist:
	global PUG_HowlingFist
	send_control(PUG_HowlingFist)
	return

cast_pug_internalrelease:
	global PUG_InternalRelease
	send_control(PUG_InternalRelease)
	return

cast_pug_mantra:
	global PUG_Mantra
	send_control(PUG_Mantra)
	return

cast_pug_perfectbalance:
	global PUG_PerfectBalance
	send_control(PUG_PerfectBalance)
	return

cast_pug_secondwind:
	global PUG_SecondWind
	send_control(PUG_SecondWind)
	return

cast_pug_snappunch:
	global PUG_SnapPunch
	send_control(PUG_SnapPunch)
	GoSub enable_greased_lightning
	SetTimer, disable_greased_lightning, -15000
	return

cast_pug_steelpeak:
	global PUG_SteelPeak
	send_control(PUG_SteelPeak)
	return

cast_pug_touchofdeath:
	global PUG_TouchOfDeath
	send_control(PUG_TouchOfDeath)
	return

cast_pug_truestrike:
	global PUG_TrueStrike
	send_control(PUG_TrueStrike)
	return

cast_pug_twinsnakes:
	global PUG_TwinSnakes
	send_control(PUG_TwinSnakes)
	return
