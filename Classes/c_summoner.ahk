#Persistent
;
; Summoner
;
cast_smn_enkindle:
  global SMN_Enkindle
  send_control(SMN_Enkindle)
  return

cast_smn_fester:
  global SMN_Fester
  send_control(SMN_Fester)
  return

cast_smn_spur:
  global SMN_Spur
  send_control(SMN_Spur)
  return

cast_smn_summon3:
  global SMN_Summon3
  send_control(SMN_Summon3)
  return

cast_smn_tridisaster:
  global SMN_Tridisaster
  send_control(SMN_Tridisaster)
  return
