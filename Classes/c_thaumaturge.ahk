#Persistent
;
; Thaumaturge
;

cast_thm_aetherial_manipulation:
	global THM_AetherialManipulation
	send_control(THM_AetherialManipulation)
	return

cast_thm_blizzard:
	global THM_Blizzard
	send_control(THM_Blizzard)
	return

cast_thm_blizzard2:
	global THM_Blizzard2
	send_control(THM_Blizzard2)
	return

cast_thm_blizzard3:
	global THM_Blizzard3
	send_control(THM_Blizzard3)
	return

cast_thm_fire:
	global THM_Fire
	send_control(THM_Fire)
	return

cast_thm_fire2:
	global THM_Fire2
	send_control(THM_Fire2)
	return

cast_thm_fire3:
	global THM_Fire3
	send_control(THM_Fire3)
	return

cast_thm_lethargy:
	global THM_Lethargy
	send_control(THM_Lethargy)
	return

cast_thm_manaward:
	global THM_Manaward
	send_control(THM_Manaward)
	return

cast_thm_scathe:
	global THM_Scathe
	send_control(THM_Scathe)
	return

cast_thm_sleep:
	global THM_Sleep
	send_control(THM_Sleep)
	return

cast_thm_surecast:
	global THM_Surecast
	send_control(THM_Surecast)
	return

cast_thm_swiftcast:
	global THM_Swiftcast
	send_control(THM_Swiftcast)
	return
	
cast_thm_thunder:
	global THM_Thunder
	send_control(THM_Thunder)
	return

cast_thm_thunder2:
	global THM_Thunder2
	send_control(%THM_Thunder2%)
	return

cast_thm_thunder3:
	global THM_Thunder3
	send_control(THM_Thunder3)
	return

cast_thm_transpose:
	global THM_Transpose
	send_control(THM_Transpose)
	return
