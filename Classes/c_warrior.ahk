#Persistent
;
; Warrior
;
cast_war_defiance:
  global WAR_Defiance
  send_control(WAR_Defiance)
  return

cast_war_infuriate:
  global WAR_Infuriate
  send_control(WAR_Infuriate)
  return

cast_war_innerbeast:
  global WAR_InnerBeast
  send_control(WAR_InnerBeast)
  return

cast_war_steelcyclone:
  global WAR_SteelCyclone
  send_control(WAR_SteelCyclone)
  return

cast_war_unchained:
  global WAR_Unchained
  send_control(WAR_Unchained)
  return
