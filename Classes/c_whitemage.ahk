#Persistent
;
; Whitemage
;
cast_wmg_benediction:
  global WMG_Benediction
  send_control(WMG_Benediction)
  return

cast_wmg_divineseal:
  global WMG_DivineSeal
  send_control(WMG_DivineSeal)
  return

cast_wmg_holy:
  global WMG_Holy
  send_control(WMG_Holy)
  return

cast_wmg_presenceofmind:
  global WMG_PresenceOfMind
  send_control(WMG_PresenceOfMind)
  return

cast_wmg_regen:
  global WMG_Regen
  send_control(WMG_Regen)
  return
