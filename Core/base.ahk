#Persistent
#Include %A_ScriptDir%\Core\Timer.ahk
;; Final Fantasy FFXIV Helpers
WinGet, GameID, ID, ahk_class FFXIVGAME

GreasedLightningEnabled := false
BaseCooldown = 2501

CooldownMulti = 1.0
BaseDelay := (BaseCooldown - CooldownReduction) * -1
LatencyModifier = -20
HalfDelay   := ((BaseDelay * CooldownMulti) / 2) + LatencyModifier
DoubleDelay := (BaseDelay * 2 * CooldownMulti) + LatencyModifier
TripleDelay := (BaseDelay * 3 * CooldownMulti) + LatencyModifier
QuadraDelay := (BaseDelay * 4 * CooldownMulti) + LatencyModifier
PentaDelay  := (BaseDelay * 5 * CooldownMulti) + LatencyModifier
BaseDelay *= CooldownMulti
BaseDelay += LatencyModifier

; Reload Support
F9::
	WinGet, GameID, ID, ahk_class FFXIVGAME
	Reload
	WinGet, GameID, ID, ahk_class FFXIVGAME
	return

control_up:
	global GameID
	ControlSend,, {Ctrl Up}, ahk_id %GameID%
	return

send_control(KeyCombo) {
	global GameID
	ControlSend,, %KeyCombo%, ahk_id %GameID%
	return
}

#Include %A_ScriptDir%\Classes\c_arcanist.ahk
#Include %A_ScriptDir%\Classes\c_archer.ahk
#Include %A_ScriptDir%\Classes\c_bard.ahk
#Include %A_ScriptDir%\Classes\c_blackmage.ahk
#Include %A_ScriptDir%\Classes\c_conjurer.ahk
#Include %A_ScriptDir%\Classes\c_dragoon.ahk
#Include %A_ScriptDir%\Classes\c_gladiator.ahk
#Include %A_ScriptDir%\Classes\c_lancer.ahk
#Include %A_ScriptDir%\Classes\c_marauder.ahk
#Include %A_ScriptDir%\Classes\c_monk.ahk
#Include %A_ScriptDir%\Classes\c_paladin.ahk
#Include %A_ScriptDir%\Classes\c_pugilist.ahk
#Include %A_ScriptDir%\Classes\c_summoner.ahk
#Include %A_ScriptDir%\Classes\c_thaumaturge.ahk
#Include %A_ScriptDir%\Classes\c_warrior.ahk
#Include %A_ScriptDir%\Classes\c_whitemage.ahk

;
; General
;

enable_greased_lightning:
	if !GreasedLightningEnabled {
		CooldownMulti = 0.95
		recalc_cooldowns()
		GreasedLightningEnabled := true
	}
	return

disable_greased_lightning:
	CooldownMulti = 1.0
	recalc_cooldowns()
	GreasedLightningEnabled := false
	return

recalc_cooldowns() {
	global
	BaseDelay := (BaseCooldown - CooldownReduction) * -1
	LatencyModifier = -20
	HalfDelay   := ((BaseDelay * CooldownMulti) / 2) + LatencyModifier
	DoubleDelay := (BaseDelay * 2 * CooldownMulti) + LatencyModifier
	TripleDelay := (BaseDelay * 3 * CooldownMulti) + LatencyModifier
	QuadraDelay := (BaseDelay * 4 * CooldownMulti) + LatencyModifier
	PentaDelay  := (BaseDelay * 5 * CooldownMulti) + LatencyModifier
	BaseDelay *= CooldownMulti
	BaseDelay += LatencyModifier
	return
}

cast_spell(ClassAbbr, SpellName) {
	GoSub cast_%ClassAbbr%_%SpellName%
	return	
}
