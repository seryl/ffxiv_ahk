#Persistent

; Gladiator
GLA_Awareness             = {3 Down}{3 Up}
GLA_Bulwark               = {3 Down}{3 Up}
GLA_CircleOfScorn         = {3 Down}{3 Up}
GLA_Convalesence          = {3 Down}{3 Up}
GLA_FastBlade             = {3 Down}{3 Up}
GLA_FightOrFlight         = {3 Down}{3 Up}
GLA_Flash                 = {3 Down}{3 Up}
GLA_Provoke               = {3 Down}{3 Up}
GLA_RageOfHalone          = {3 Down}{3 Up}
GLA_Rampart               = {3 Down}{3 Up}
GLA_RiotBlade             = {3 Down}{3 Up}
GLA_SavageBlade           = {3 Down}{3 Up}
GLA_Sentinel              = {3 Down}{3 Up}
GLA_ShieldBash            = {3 Down}{3 Up}
GLA_ShieldLob             = {3 Down}{3 Up}
GLA_ShieldSwipe           = {3 Down}{3 Up}
GLA_TemperedWill          = {3 Down}{3 Up}

; Paladin
PAL_Cover                 = {3 Down}{3 Up}
PAL_HallowedGround        = {3 Down}{3 Up}
PAL_ShieldOath            = {3 Down}{3 Up}
PAL_SpiritsWithin         = {3 Down}{3 Up}
PAL_SwordOath             = {3 Down}{3 Up}

; Marauder
MAR_Berserk               = {3 Down}{3 Up}
MAR_Bloodbath             = {3 Down}{3 Up}
MAR_BrutalSwing           = {3 Down}{3 Up}
MAR_ButchersBlock         = {3 Down}{3 Up}
MAR_Foresight             = {3 Down}{3 Up}
MAR_Fracture              = {3 Down}{3 Up}
MAR_HeavySwing            = {3 Down}{3 Up}
MAR_Maim                  = {3 Down}{3 Up}
MAR_MercyStroke           = {3 Down}{3 Up}
MAR_Overpower             = {3 Down}{3 Up}
MAR_SkullSunder           = {3 Down}{3 Up}
MAR_ThrillOfBattle        = {3 Down}{3 Up}
MAR_Tomahawk              = {3 Down}{3 Up}

; Warrior
WAR_Defiance              = {3 Down}{3 Up}
WAR_Infuriate             = {3 Down}{3 Up}
WAR_InnerBeast            = {3 Down}{3 Up}
WAR_SteelCyclone          = {3 Down}{3 Up}
WAR_Unchained             = {3 Down}{3 Up}

; Pugilist
PUG_ArmOfTheDestroyer     = {3 Down}{3 Up}
PUG_Bootshine             = {Ctrl down}{Numpad1 Down}{Numpad1 Up}{Ctrl up}
PUG_Demolish              = {3 Down}{3 Up}
PUG_Featherfoot           = {3 Down}{3 Up}
PUG_FistsOfEarth          = {3 Down}{3 Up}
PUG_FistsOfWind           = {3 Down}{3 Up}
PUG_Haymaker              = {3 Down}{3 Up}
PUG_HowlingFist           = {3 Down}{3 Up}
PUG_InternalRelease       = {1 Down}{1 Up}
PUG_Mantra                = {3 Down}{3 Up}
PUG_PerfectBalance        = {3 Down}{3 Up}
PUG_SecondWind            = {3 Down}{3 Up}
PUG_SnapPunch             = {Ctrl down}{Numpad3 Down}{Numpad3 Up}{Ctrl up}
PUG_SteelPeak             = {3 Down}{3 Up}
PUG_TouchOfDeath          = ^{2 Down}^{2 Up}
PUG_TrueStrike            = {Ctrl down}{Numpad2 Down}{Numpad2 Up}{Ctrl up}
PUG_TwinSnakes            = {Ctrl down}{Numpad6 Down}{Numpad6 Up}{Ctrl up}

; Monk
MNK_DragonKick            = {3 Down}{3 Up}
MNK_FistsOfFire           = {3 Down}{3 Up}
MNK_OneIlmPunch           = {3 Down}{3 Up}
MNK_Rockbreaker           = {3 Down}{3 Up}
MNK_ShoulderTackle        = {3 Down}{3 Up}

; Lancer
LNC_BloodForBlood         = {3 Down}{3 Up}
LNC_ChaosThrust           = {3 Down}{3 Up}
LNC_Disembowel            = {3 Down}{3 Up}
LNC_DoomSpike             = {3 Down}{3 Up}
LNC_Feint                 = {3 Down}{3 Up}
LNC_FullThrust            = {3 Down}{3 Up}
LNC_HeavyThrust           = {3 Down}{3 Up}
LNC_ImpulseDrive          = {3 Down}{3 Up}
LNC_Invigorate            = {3 Down}{3 Up}
LNC_KeenFlurry            = {3 Down}{3 Up}
LNC_LegSweep              = {3 Down}{3 Up}
LNC_LifeSurge             = {3 Down}{3 Up}
LNC_Phlebotomize          = {3 Down}{3 Up}
LNC_PiercingTalon         = {3 Down}{3 Up}
LNC_RingOfThorns          = {3 Down}{3 Up}
LNC_TrueThrust            = {3 Down}{3 Up}
LNC_VorpalThrust          = {3 Down}{3 Up}

; Dragoon
DRG_DragonfireDive        = {3 Down}{3 Up}
DRG_ElusiveJump           = {3 Down}{3 Up}
DRG_Jump                  = {3 Down}{3 Up}
DRG_PowerSurge            = {3 Down}{3 Up}
DRG_SpineshatterDive      = {3 Down}{3 Up}

; Archer
ARC_Barrage               = {3 Down}{3 Up}
ARC_Bloodletter           = {3 Down}{3 Up}
ARC_BluntArrow            = {3 Down}{3 Up}
ARC_FlamingArrow          = {3 Down}{3 Up}
ARC_HawksEye              = {3 Down}{3 Up}
ARC_HeavyShot             = {1 Down}{1 Up}
ARC_MiserysEnd            = {3 Down}{3 Up}
ARC_QuellingStrikes       = {3 Down}{3 Up}
ARC_QuickNock             = {3 Down}{3 Up}
ARC_RagingStrikes         = {3 Down}{3 Up}
ARC_RepellingShot         = {3 Down}{3 Up}
ARC_Shadowbind            = {3 Down}{3 Up}
ARC_StraightShot          = {2 Down}{2 Up}
ARC_Swiftsong             = {3 Down}{3 Up}
ARC_VenomousBite          = {4}
ARC_WideVolley            = {3 Down}{3 Up}
ARC_Windbite              = {3 Down}{3 Up}

; Bard
BRD_ArmysPaeon            = {3 Down}{3 Up}
BRD_BattleVoice           = {3 Down}{3 Up}
BRD_FoeRequiem            = {3 Down}{3 Up}
BRD_MagesBallad           = {3 Down}{3 Up}
BRD_RainOfDeath           = {3 Down}{3 Up}

; Conjurer
CNJ_Aero                  = {2 Down}{2 Up}
CNJ_Aero2                 = {3 Down}{3 Up}
CNJ_ClericStance          = {3 Down}{3 Up}
CNJ_Cure                  = {3 Down}{3 Up}
CNJ_Cure2                 = {3 Down}{3 Up}
CNJ_Cure3                 = {3 Down}{3 Up}
CNJ_Esuna                 = {3 Down}{3 Up}
CNJ_FluidAura             = {3 Down}{3 Up}
CNJ_Medica                = {3 Down}{3 Up}
CNJ_Medica2               = {3 Down}{3 Up}
CNJ_Protect               = {3 Down}{3 Up}
CNJ_Raise                 = {3 Down}{3 Up}
CNJ_Repose                = {3 Down}{3 Up}
CNJ_ShroudOfSaints        = {3 Down}{3 Up}
CNJ_Stone                 = {1 Down}{1 Up}
CNJ_Stone2                = {3 Down}{3 Up}
CNJ_Stoneskin             = {3 Down}{3 Up}

; White Mage
WMG_Benediction           = {3 Down}{3 Up}
WMG_DivineSeal            = {3 Down}{3 Up}
WMG_Holy                  = {3 Down}{3 Up}
WMG_PresenceOfMind        = {3 Down}{3 Up}
WMG_Regen                 = {3 Down}{3 Up}

; Thaumaturge
THM_AetherialManipulation = {3 Down}{3 Up}
THM_Blizzard              = {2 Down}{2 Up}
THM_Blizzard2             = {3 Down}{3 Up}
THM_Blizzard3             = {3 Down}{3 Up}
THM_Fire                  = {Ctrl down}{Numpad1 Down}{Numpad1 Up}{Ctrl up}
THM_Fire2                 = {Ctrl down}{Numpad1 Down}{Numpad1 Up}{Ctrl up}
THM_Fire3                 = {Ctrl down}{Numpad1 Down}{Numpad1 Up}{Ctrl up}
THM_Lethargy              = {6 Down}{6 Up}
THM_Manaward              = {3 Down}{3 Up}
THM_Scathe                = ^{1 Down}^{1 Up}
THM_Sleep                 = {5 Down}{5 Up}
THM_Surecast              = {6 Down}{6 Up}
THM_Swiftcast             = {6 Down}{6 Up}
THM_Thunder               = {6 Down}{6 Up}
THM_Thunder2              = {6 Down}{6 Up}
THM_Thunder3              = {6 Down}{6 Up}
THM_Transpose             = ^{2 Down}^{2 Up}

; Black Mage
BMG_Apocatastasis         = {3 Down}{3 Up}
BMG_Convert               = {3 Down}{3 Up}
BMG_Flare                 = {3 Down}{3 Up}
BMG_Freeze                = {3 Down}{3 Up}
BMG_Manawall              = {3 Down}{3 Up}

; Arcanist
ARC_Bane                  = {3 Down}{3 Up}
ARC_Bio                   = {3 Down}{3 Up}
ARC_Bio2                  = {3 Down}{3 Up}
ARC_EnergyDrain           = {3 Down}{3 Up}
ARC_EyeForAnEye           = {3 Down}{3 Up}
ARC_Miasma                = {3 Down}{3 Up}
ARC_Miasma2               = {3 Down}{3 Up}
ARC_Physick               = {3 Down}{3 Up}
ARC_Resurrection          = {3 Down}{3 Up}
ARC_Rouse                 = {3 Down}{3 Up}
ARC_Ruin                  = {3 Down}{3 Up}
ARC_Ruin2                 = {3 Down}{3 Up}
ARC_ShadowFlare           = {3 Down}{3 Up}
ARC_Summon                = {3 Down}{3 Up}
ARC_Summon2               = {3 Down}{3 Up}
ARC_Sustain               = {3 Down}{3 Up}
ARC_Virus                 = {3 Down}{3 Up}

; Summoner
SMN_Enkindle              = {3 Down}{3 Up}
SMN_Fester                = {3 Down}{3 Up}
SMN_Spur                  = {3 Down}{3 Up}
SMN_Summon3               = {3 Down}{3 Up}
SMN_Tridisaster           = {3 Down}{3 Up}

; Globals
CooldownReduction = 0
