﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
#InstallKeybdHook
#MaxThreads 255
#MaxThreadsPerHotkey 20
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Play  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
process, priority, ,high
WinGet, GameID, ID, ahk_class FFXIVGAME

#Include %A_ScriptDir%\Core\keybinds.ahk

#Include %A_ScriptDir%\Core\base.ahk


^xbutton2::
	cast_spell("arc", "miserysend")
	return

xbutton2::
	cast_spell("arc", "straightshot")
	return

xbutton1::
	cast_spell("arc", "heavyshot")
	return

^xbutton1::
	cast_spell("arc", "straightshot")
	return


