﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
#InstallKeybdHook
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

WinGet, GameID, ID, ahk_class FFXIVGAME

xbutton1::
  SetKeyDelay, 2800
  ControlSend, , 1, ahk_id %GameID%
  ControlSend, , 2, ahk_id %GameID%
  ControlSend, , 2, ahk_id %GameID%
  ControlSend, , 3, ahk_id %GameID%
  ControlSend, , 2, ahk_id %GameID%
  ControlSend, , 2, ahk_id %GameID%
  ControlSend, , 2, ahk_id %GameID%
  ControlSend, , 1, ahk_id %GameID%
  return

;xbutton2::
;  ControlSend, , ^+5, ahk_id %GameID%
;  return
