﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
#InstallKeybdHook
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

WinGet, GameID, ID, ahk_class FFXIVGAME

^xbutton1::
  BreakLoop = 1
  return

xbutton1::
  BreakLoop = 0
  SetKeyDelay, 100
  Loop {
    if (BreakLoop = 1)
	  break
    ControlSend, , 2, ahk_id %GameID%
	ControlSend, , 2, ahk_id %GameID%
	ControlSend, , 2, ahk_id %GameID%
    Sleep 14800
    ControlSend, , 3, ahk_id %GameID%
	ControlSend, , 3, ahk_id %GameID%
	ControlSend, , 3, ahk_id %GameID%
	ControlSend, , {Ctrl Up}, ahk_id %GameID%
    Sleep 15000
  }
  return

;xbutton2::
;  ControlSend, , ^+5, ahk_id %GameID%
;  return
