﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
#InstallKeybdHook
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

WinGet, GameID, ID, ahk_class FFXIVGAME

xbutton1::
	SetKeyDelay, 100
	ControlSend, , ^{Numpad1}, ahk_id %GameID%
	ControlSend, , ^{Numpad1}, ahk_id %GameID%
	SetKeyDelay, 2301
	ControlSend, , ^{Numpad1}, ahk_id %GameID%

	SetKeyDelay, 100
	ControlSend, , ^{Numpad2}, ahk_id %GameID%
	ControlSend, , ^{Numpad2}, ahk_id %GameID%
	SetKeyDelay, 2301
	ControlSend, , ^{Numpad2}, ahk_id %GameID%

	SetKeyDelay, 100
	ControlSend, , ^{Numpad3}, ahk_id %GameID%
	ControlSend, , ^{Numpad3}, ahk_id %GameID%
	ControlSend, , ^{Numpad3}, ahk_id %GameID%
	ControlSend, , {Ctrl Up}, ahk_id %GameID%
	return

;xbutton2::
	ControlSend, , ^{Numpad5}, ahk_id %GameID%
	return