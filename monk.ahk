﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
#InstallKeybdHook
#MaxThreads 255
#MaxThreadsPerHotkey 20
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Play  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
process, priority, ,high
WinGet, GameID, ID, ahk_class FFXIVGAME

#Include %A_ScriptDir%\Core\keybinds.ahk

; Globals
MAR_Fracture = {2 Down}{2 Up}

BaseCooldown = 2501
CooldownReduction = 30

#Include %A_ScriptDir%\Core\base.ahk

xbutton1::
	cast_spell("pug", "bootshine")
	if Timer("TwinSnakes") {
		SetTimer, cast_pug_twinsnakes, %BaseDelay%
		Timer("TwinSnakes", 15000)
	} else {
		SetTimer, cast_pug_truestrike, %BaseDelay%
	}
	SetTimer, cast_pug_snappunch, %DoubleDelay%
	return

^xbutton1::
	cast_spell("pug", "internalrelease")
	return

xbutton2::
	cast_spell("pug", "touchofdeath")
	SetTimer, cast_mar_fracture, %BaseDelay%
	return
